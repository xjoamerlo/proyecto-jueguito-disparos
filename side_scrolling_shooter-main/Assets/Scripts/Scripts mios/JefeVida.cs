using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeVida : MonoBehaviour
{
    private int WasHit = 0; // Contador de balas que golpearon al jefe
    public int RequiredBullets = 40; // Numero de balas requeridas para matar al jefe
    void Start()
    {
        
    }

    void OnCollisionEnter(Collision collision) /// Verifica las colisiones
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            WasHit++;
            if (WasHit >= RequiredBullets)
            {
                Destroy(gameObject);
            }
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            WasHit++;

            if (WasHit >= RequiredBullets)
            {
                Destroy(gameObject);
            }
        }
    }
}