using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaJefe : MonoBehaviour
{
    public float bulletLife = 25f;  
    public float rotation = 0f;
    public float speed = 3f;
    private Vector3 spawnPoint;
    private float timer = 0f;
    void Start()
    {
        spawnPoint = new Vector3(transform.position.x, transform.position.y);
    }
    void Update()
    {
        if (timer > bulletLife) Destroy(this.gameObject);
        timer += Time.deltaTime;
        transform.position = Movement(timer);
    }
    private Vector3 Movement(float timer)
    {
        float x = timer * speed * transform.right.x;
        float y = timer * speed * transform.right.y;
        return new Vector3(x + spawnPoint.x, y + spawnPoint.y);
    }
    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
        }
    }
}
