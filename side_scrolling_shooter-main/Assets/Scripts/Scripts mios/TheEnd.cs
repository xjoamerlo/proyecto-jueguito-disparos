using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class TheEnd : MonoBehaviour
{

    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Time.timeScale = 1;
            SceneManager.LoadSceneAsync("TheEnd");
        }
    }
}
